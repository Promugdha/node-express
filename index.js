const express = require('express');
const http = require('http');
const morgan = require('morgan');
const bodyparser = require('body-parser');
const dishRoute = require('./routers/dishRoute');
const promoRoute = require('./routers/promoRoute');
const leaderRoute = require('./routers/leaderRoute');

const port = 5000;
const hostname = 'localhost';

const app = express();

app.use(morgan('dev'));

app.use(bodyparser.json());

app.use('/dishes', dishRoute);

app.use('/promos', promoRoute);

app.use('/lead', leaderRoute);

app.use(express.static(__dirname + '/public'));

app.use((req, res, next) => {

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.end('<html><h1>this is a server of express</h1></html>');

})

const server = http.createServer(app);
server.listen(port, hostname, () => {
    console.log(`Serving at http://${hostname}:${port}`)
})