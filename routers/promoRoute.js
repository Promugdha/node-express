const express = require('express');
const bodyparser = require('body-parser');

const promoRoute = express.Router();

promoRoute.use(bodyparser.json());

promoRoute.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();
    })
    .get((req, res, next) => {
        res.end("Displaying all promotions");
    })
    .post((req, res, next) => {
        res.end("sending promotion " + req.body.name + " which is like " + req.body.description);
    })
    .put((req, res, next) => {
        res.statusCode = 403;
        res.end(req.method + " not possible");
    })
    .delete((req, res, next) => {
        res.end("deleting all items")
    })
promoRoute.route('/:promoid')
    .get((req, res, next) => {
        res.end("fetching promotions " + req.params.promoid);
    })
    .post((req, res, next) => {
        res.statusCode = 403
        res.end(req.method + " not possible");
    })
    .put((req, res, next) => {
        res.end("replacing existing promotion with " + req.body.name + " which is like " + req.body.description);
    })
    .delete((req, res, next) => {
        res.end("deleting item")
    })

module.exports = promoRoute;    