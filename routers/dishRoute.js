const express = require('express');
const bodyparser = require('body-parser');

const dishRoute = express.Router();

dishRoute.use(bodyparser.json());

dishRoute.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Content-Type", "text/plain");
        next();
    })
    .get((req, res, next) => {
        res.end("will send dishes");
    })
    .post((req, res, next) => {
        res.end("will send dish " + req.body.name + "which is like " + req.body.description);
    })
    .put((req, res, next) => {
        res.statusCode = 403;
        res.end("operation not possible");
    })
    .delete((req, res, next) => {
        res.end("deleting dishes");
    });
dishRoute.route('/:dishid')
    .get((req, res, next) => {
        res.end("will send dishes " + req.params.dishid);
    })
    .post((req, res, next) => {
        res.statusCode = 403;
        res.end("operation not possible");
    })
    .put((req, res, next) => {
        res.end("will replace dish " + req.body.name + "which is like " + req.body.description);
    })
    .delete((req, res, next) => {
        res.end("deleting dishes" + req.params.dishid);
    });

module.exports = dishRoute;