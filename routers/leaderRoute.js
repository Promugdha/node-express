const express = require('express');
const bodyparser = require('body-parser');

const leaderRoute = express.Router();

leaderRoute.use(bodyparser.json());

leaderRoute.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();
    })
    .get((req, res, next) => {
        res.end("Displaying all leaders");
    })
    .post((req, res, next) => {
        res.end("sending leaders " + req.body.name + " who is like " + req.body.description);
    })
    .put((req, res, next) => {
        res.statusCode = 403;
        res.end(req.method + " not possible");
    })
    .delete((req, res, next) => {
        res.end("deleting all items")
    })
leaderRoute.route('/:leadid')
    .get((req, res, next) => {
        res.end("fetching leaders " + req.params.leadid);
    })
    .post((req, res, next) => {
        res.statusCode = 403
        res.end(req.method + " not possible");
    })
    .put((req, res, next) => {
        res.end("replacing existing leader with " + req.body.name + " which is like " + req.body.description);
    })
    .delete((req, res, next) => {
        res.end("deleting item")
    })

module.exports = leaderRoute;    